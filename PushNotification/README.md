# Push Notification Module

Use it like this:
```javascript
pushnotifications.PushNotifications.RequestPermissions();

pushnotifications.PushNotifications.SetApplicationBadge(1);

pushnotifications.PushNotifications.ResetApplicationBadge();

pushnotifications.PushNotifications.ScheduleNotification("Hello World!", 60,true);

pushnotifications.PushNotifications.ScheduleNotificationAndSetBadge("Hello World!", 60, 4);
```
