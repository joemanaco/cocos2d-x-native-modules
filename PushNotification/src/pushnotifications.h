#ifndef  _PUSH_NOTIFICATIONS_H_
#define  _PUSH_NOTIFICATIONS_H_

#include <string>

class PushNotifications
{
public:
    void static RequestPermissions();
    void static SetApplicationBadge(int number);
    void static ScheduleNotification(std::string msg, int timeInSeconds, bool incrementBadge);
    void static ScheduleNotificationAndSetBadge(std::string msg, int timeInSeconds, int badge);
    void static ResetApplicationBadge();
    void static ClearAllNotifications();
};


#endif // _PUSH_NOTIFICATIONS_H_
