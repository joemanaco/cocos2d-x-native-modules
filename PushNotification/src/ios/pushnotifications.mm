#include "pushnotifications.h"

void PushNotifications::RequestPermissions() 
{
    UIUserNotificationSettings* notificationSettings = [UIUserNotificationSettings settingsForTypes:UIUserNotificationTypeAlert | UIUserNotificationTypeBadge | UIUserNotificationTypeSound categories:nil];
    if (notificationSettings != nil) {
        [[UIApplication sharedApplication] registerUserNotificationSettings:notificationSettings];
    }
}

void PushNotifications::SetApplicationBadge(int number)
{
    [UIApplication sharedApplication].applicationIconBadgeNumber = number;
}

void PushNotifications::ScheduleNotification(std::string msg, int timeInSeconds, bool incrementBadge)
{

    UILocalNotification* localNotification = [[UILocalNotification alloc] init];
    localNotification.fireDate = [NSDate dateWithTimeIntervalSinceNow: timeInSeconds];
    localNotification.alertBody = [NSString stringWithCString:msg.c_str() encoding:[NSString defaultCStringEncoding]];
    localNotification.timeZone = [NSTimeZone defaultTimeZone];

    localNotification.soundName = UILocalNotificationDefaultSoundName;

    if (incrementBadge)
        localNotification.applicationIconBadgeNumber = [[UIApplication sharedApplication] applicationIconBadgeNumber] + 1;

    [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
}

void PushNotifications::ScheduleNotificationAndSetBadge(std::string msg, int timeInSeconds, int badge)
{
    UILocalNotification* localNotification = [[UILocalNotification alloc] init];
    localNotification.fireDate = [NSDate dateWithTimeIntervalSinceNow: timeInSeconds];
    localNotification.alertBody = [NSString stringWithCString:msg.c_str() encoding:[NSString defaultCStringEncoding]];
    localNotification.timeZone = [NSTimeZone defaultTimeZone];

    localNotification.soundName = UILocalNotificationDefaultSoundName;

    localNotification.applicationIconBadgeNumber = badge;

    [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
}

void PushNotifications::ResetApplicationBadge()
{
    SetApplicationBadge(0);
}

void PushNotifications::ClearAllNotifications()
{
    [[UIApplication sharedApplication] cancelAllLocalNotifications];
}