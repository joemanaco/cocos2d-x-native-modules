/**
 * @module pushnotifications
 */
var pushnotifications = pushnotifications || {
    PushNotifications: {
        SetApplicationBadge : function (int) { cc.log("[PushNotifications] Set Badge to: " + int); },
        ResetApplicationBadge : function () {},
        ScheduleNotification : function (str, time, bool) {
            cc.log("[PushNotifications]Schedule: " + str + " in " + time + " seconds"); 
        },
        RequestPermissions : function (){},
        ClearAllNotifications : function (){
            cc.log( "[PushNotifications]clear all notifications");
        },
        ScheduleNotificationAndSetBadge : function (str, time, badge ){
            cc.log("[PushNotifications]Schedule: " + str + " in " + time + " seconds");
        }
    }
};
