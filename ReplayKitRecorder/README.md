# ReplayKit Module

Use it like this:
```javascript
if (replaykitrecorder.ReplayKitRecorder.isAvailable()) {
    replaykitrecorder.ReplayKitRecorder.startRecordingWithMicrophoneEnabled(true);
    
    var micro = replaykitrecorder.ReplayKitRecorder.isMicrophoneEnabled();
    
    var isRecording = replaykitrecorder.ReplayKitRecorder.isRecording();
    
    replaykitrecorder.ReplayKitRecorder.stopRecording();
}

// Register Callbacks
ReplayKitListener.didStopRecordingWithError = function() {};
ReplayKitListener.previewControllerDidFinishe = function() {};
ReplayKitListener.didFinishWithActivityTypes = function() {};
ReplayKitListener.screenRecorderDidChangeAvailability = function() {};
```
