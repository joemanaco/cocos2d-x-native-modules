#ifndef _REPLAYKIT_H_
#define _REPLAYKIT_H_

class ReplayKitRecorder
{
public:
    static bool isAvailable();
    static bool isRecording();
    static bool isMicrophoneEnabled();
    static void startRecordingWithMicrophoneEnabled(bool microphoneEnabled);
    static bool stopRecording();
};

#endif /* _REPLAYKIT_H_ */
