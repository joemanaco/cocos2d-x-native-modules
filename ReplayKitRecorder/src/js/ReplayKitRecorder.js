/**
 * @module leaderboard
 */
var replaykitrecorder = replaykitrecorder || {
    ReplayKitRecorder: {
        isMicrophoneEnabled : function () { return false; },
        isRecording : function () { return false; },
        stopRecording : function () { return false; },
        startRecordingWithMicrophoneEnabled : function (bool) { },
        isAvailable : function () { return false; }
    },
    ReplayKitDelegateRecorder: {
        didStopRecordingWithError : function () {},
        previewControllerDidFinishe : function (){},
        didFinishWithActivityTypes : function (){},
        screenRecorderDidChangeAvailability : function (){}
    }
};

var ReplayKitListener = ReplayKitListener || {
    previewControllerDidFinish: function() {}
};
