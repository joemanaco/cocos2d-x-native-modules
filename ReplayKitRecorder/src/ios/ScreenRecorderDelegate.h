#import <ReplayKit/ReplayKit.h>

@interface ScreenRecorderDelegate : NSObject <RPScreenRecorderDelegate, RPPreviewViewControllerDelegate> {
}
- (void) executeJS:(const char*) methodName;
@end
