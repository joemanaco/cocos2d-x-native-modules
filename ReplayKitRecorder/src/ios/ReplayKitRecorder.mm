#import <ReplayKit/ReplayKit.h>
#include "ReplayKitRecorder.h"
#include "ScreenRecorderDelegate.h"
#include "cocos2d.h"
#include "SimpleAudioEngine.h"

ScreenRecorderDelegate* screenRecorderDelegate = [[ScreenRecorderDelegate alloc] init];

bool ReplayKitRecorder::isAvailable() {
    RPScreenRecorder* recorder =  RPScreenRecorder.sharedRecorder;
    return recorder.available;
}

bool ReplayKitRecorder::isRecording() {
    RPScreenRecorder* recorder =  RPScreenRecorder.sharedRecorder;
    return recorder.recording;
}

bool ReplayKitRecorder::isMicrophoneEnabled() {
    RPScreenRecorder* recorder =  RPScreenRecorder.sharedRecorder;
    return recorder.microphoneEnabled;
}

void ReplayKitRecorder::startRecordingWithMicrophoneEnabled(bool microphoneEnabled) {
    RPScreenRecorder* recorder =  RPScreenRecorder.sharedRecorder;
    [recorder startRecordingWithMicrophoneEnabled:YES handler:^(NSError *error) {

        if(error)
        {
            NSLog(@"RECORDER ERROR: %@", error.localizedDescription);
        }
    }];
}

bool ReplayKitRecorder::stopRecording() {
    CocosDenshion::SimpleAudioEngine::getInstance()->pauseBackgroundMusic();
    CocosDenshion::SimpleAudioEngine::getInstance()->pauseAllEffects(); 

    RPScreenRecorder* sharedRecorder =  RPScreenRecorder.sharedRecorder;
    
    [sharedRecorder stopRecordingWithHandler:^(RPPreviewViewController *previewViewController, NSError *error) {
        if (error) {
            NSLog(@"stopScreenRecording: %@", error.localizedDescription);
        }

        if (previewViewController) {
            previewViewController.previewControllerDelegate = screenRecorderDelegate;
            // RPPreviewViewController only supports full screen modal presentation.
            previewViewController.modalPresentationStyle = UIModalPresentationFullScreen;

            [[UIApplication sharedApplication].keyWindow.rootViewController presentViewController:previewViewController animated:YES completion:nil];        
        }
    }];
}
