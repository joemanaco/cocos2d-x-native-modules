#include "ScreenRecorderDelegate.h"
#include "cocos2d.h"
#include "SimpleAudioEngine.h"
#include "jsapi.h"
#include "jsfriendapi.h"
#include "ScriptingCore.h"

@implementation ScreenRecorderDelegate

- (void) executeJS:(const char*) methodName {
    // Perform Callback in Cocos-Thread because
    // Spidermonkey crashes in multithreaded environments...
    cocos2d::Director::getInstance()->getScheduler()->performFunctionInCocosThread([=](){
        ScriptingCore* sc =ScriptingCore::getInstance();

        JSContext* cx = sc->getGlobalContext();
        JS::RootedObject globalObject(cx, sc->getGlobalObject());

        JS::RootedValue nsval(sc->getGlobalContext());

        JS_GetProperty(cx, globalObject, "ReplayKitListener", &nsval);
        if (nsval != JSVAL_VOID) {
            sc->executeFunctionWithOwner(nsval, methodName, 0, nullptr);
        }else{
            CCLOG("Undefined function: ReplayKitListener.%s", methodName);
        }
    });
}

#pragma mark - RPScreenRecorderDelegate

- (void)screenRecorder:(RPScreenRecorder *)screenRecorder didStopRecordingWithError:(NSError *)error previewViewController:(nullable RPPreviewViewController *)previewViewController {
    if(error) {
        NSLog(@"RecordingError: %@", error.localizedDescription);
    }
}

- (void)screenRecorderDidChangeAvailability:(RPScreenRecorder *)screenRecorder {
    NSLog(@"screenRecorderDidChangeAvailability");
}

#pragma mark - RPPreviewViewControllerDelegate

- (void)previewControllerDidFinish:(RPPreviewViewController *)previewController {
    CocosDenshion::SimpleAudioEngine::getInstance()->resumeBackgroundMusic();
    CocosDenshion::SimpleAudioEngine::getInstance()->resumeAllEffects();
    [previewController dismissViewControllerAnimated:YES completion:nil];
    [self executeJS:"previewControllerDidFinish"];
}

- (void)previewController:(RPPreviewViewController *)previewController didFinishWithActivityTypes:(NSSet<NSString *> *)activityTypes {
    CocosDenshion::SimpleAudioEngine::getInstance()->resumeBackgroundMusic();
    CocosDenshion::SimpleAudioEngine::getInstance()->resumeAllEffects();
    [previewController dismissViewControllerAnimated:YES completion:nil];    
}

@end