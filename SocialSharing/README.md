# Social Sharing Module

Examples:

```javascript
share.url("foo", "http://www.google.de");
share.openUrl("http://www.google.de");
share.alert("Message", "This is a message", "Okay");
```
