var social = social || {
    share: {
        url: function(text, url) {           
            if (cc.sys.ANDROID) {
                jsb.reflection.callStaticMethod("com/asylumsquare/social/Share", "url", "(Ljava/lang/String;Ljava/lang/String;)V", text, url);
            } else {
                cc.log("[share] Cannot share on HTML5");
            }
        },
        openUrl: function(url) {
            if (cc.sys.ANDROID) {
                jsb.reflection.callStaticMethod("com/asylumsquare/social/Share", "openUrl", "(Ljava/lang/String;)V", url);
            } else {
                window.open(url, '_blank');
            }
        },
        alert: function(title, message, buttons) {
            alert(message);
        }
    }
};
