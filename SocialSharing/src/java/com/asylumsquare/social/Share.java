package com.asylumsquare.social;

import org.cocos2dx.javascript.AppActivity;
import android.content.Intent;
import android.app.AlertDialog;

public class Share {
    public static void url(String text, String url) {
        Intent share = new Intent(Intent.ACTION_SEND);
        share.setType("text/plain");

        share.putExtra(Intent.EXTRA_TEXT, text + " " + url);

        AppActivity.app.startActivity(Intent.createChooser(share, "Share"));    
    }

    public static void openUrl(String url) {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW,android.net.Uri.parse(url));
        AppActivity.app.startActivity(browserIntent);
    }

    public static void alert(final String title, final String message, final String buttonName)
    {
        AppActivity.app.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                AlertDialog alertDialog = new AlertDialog.Builder(AppActivity.app).create();
                alertDialog.setTitle(title);
                alertDialog.setMessage(message);
                alertDialog.show();
            }
        });
    }
}
