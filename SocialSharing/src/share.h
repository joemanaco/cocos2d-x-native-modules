#ifndef  _SOCIAL_SHARING_H_
#define  _SOCIAL_SHARING_H_

#include <string>

class share 
{
public:
    void static url(std::string text, std::string url);
    void static openUrl(std::string url);
    void static alert(std::string title, std::string message, std::string buttonName);
};

#endif // _SOCIAL_SHARING_H_
