#include "share.h";

void share::url(std::string text, std::string url)
{
    NSString *textToShare = [NSString stringWithUTF8String:text.c_str()];
    NSURL *nsUrl = [NSURL URLWithString:([NSString stringWithCString:url.c_str() encoding:[NSString defaultCStringEncoding]])];
    NSArray *activityItems = [[NSArray alloc]  initWithObjects:textToShare, nsUrl,nil];

    UIActivity *activity = [[UIActivity alloc] init];

    NSArray *applicationActivities = [[NSArray alloc] initWithObjects:activity, nil];
    UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:activityItems applicationActivities:applicationActivities];

    auto viewController = [UIApplication sharedApplication].keyWindow.rootViewController;

    if ( [activityVC respondsToSelector:@selector(popoverPresentationController)] ) {
        // iOS8 fix
        activityVC.popoverPresentationController.sourceView = viewController.view;
    }

    [activityVC setCompletionHandler:^(NSString *activityType, BOOL completed) {
        [viewController dismissModalViewControllerAnimated:YES];
    }];

    [viewController presentModalViewController:activityVC animated:YES];
}

void share::openUrl(std::string url) {
    NSURL *nsUrl = [NSURL URLWithString:([NSString stringWithCString:url.c_str() encoding:[NSString defaultCStringEncoding]])];
    if(nsUrl) [[UIApplication sharedApplication] openURL:nsUrl];
}


void share::alert(std::string title, std::string message, std::string buttonName)
{
    UIAlertView *alert = [[UIAlertView alloc]
        initWithTitle:[NSString stringWithUTF8String:title.c_str()]
        message:[NSString stringWithUTF8String:message.c_str()]
        delegate:nil
        cancelButtonTitle:[NSString stringWithUTF8String:buttonName.c_str()]
        otherButtonTitles:nil];

    [alert show];
    [alert release];
}




