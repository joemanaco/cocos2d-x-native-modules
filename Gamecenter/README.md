# Gamecenter Module

Example:

```javascript
leaderboard.GameCenter.login();

leaderboard.GameCenter.postScore(highScoreId, playerHighScore);

leaderboard.GameCenter.showScores(highScoreId);
```
