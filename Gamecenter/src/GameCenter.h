#ifndef _GAMECENTER_H
#define _GAMECENTER_H

class GameCenter
{
public:
    static void login();

    static bool showAchievements();
    static void postAchievement(const char* idName, int percentComplete);
    static void clearAllAchievements();

    static bool showScores(const char* idName);
    static void postScore(const char* idName, int score);
    static void clearAllScores();
};

#endif /* _GAMECENTER_H */
