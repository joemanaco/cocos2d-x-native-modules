/**
 * @module leaderboard
 */
var leaderboard = leaderboard || {
    GameCenter: {
        postAchievement : function (char, int) { 
            cc.log("[GameCenter] Post Achievement: " + char +  " / " + int)
        },
        showScores : function (scoreId) { cc.log("[GameCenter] Show Scores " + scoreId); return false; },
        clearAllScores : function () { cc.log("[GameCenter] Clear All Scores"); },
        showAchievements : function () { cc.log("[GameCenter] Show Achievements") ; return false; },
        postScore : function (char, int) { cc.log("[GameCenter] Post Score: " + char + " / " + int);},
        login : function () { cc.log("[GameCenter] login"); },
        clearAllAchievements : function () { cc.log("[GameCenter] clear all achievements"); }
    }
};
