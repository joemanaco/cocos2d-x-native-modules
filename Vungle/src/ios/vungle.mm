#import <VungleSDK/VungleSDK.h>
#include "cocos2d.h"

@interface VungleDelegate : UIResponder <UIApplicationDelegate, VungleSDKDelegate>
{
}
@end

@implementation VungleDelegate

- (id) init
{
    if (self = [super init])
    {
    }
    return self;
}

- (void) dealloc
{
    [super dealloc];
}

- (void)vungleSDKwillCloseAdWithViewInfo:(NSDictionary*)viewInfo willPresentProductSheet:(BOOL)willPresentProductSheet {
    cocos2d::Application::getInstance()->applicationWillEnterForeground();
}

- (void)vungleSDKwillShowAd {
    cocos2d::Application::getInstance()->applicationDidEnterBackground();
}

- (void)vungleSDKhasCachedAdAvailable {
}
@end

#include "Vungle/vungle.h"

VungleDelegate *delegate;

void Vungle::Init(const char * id) 
{
    delegate = [[[VungleDelegate alloc] init] retain];

    NSString* appId = [NSString stringWithUTF8String:id];

    VungleSDK* sdk = [VungleSDK sharedSDK];
    [sdk setDelegate: delegate];
    // start vungle publisher library
    [sdk startWithAppId:appId];
}

bool Vungle::ShowFullscreen() 
{
    if ([[VungleSDK sharedSDK] isAdPlayable]) {
        NSError* error = nil;
        [[VungleSDK sharedSDK] playAd:[UIApplication sharedApplication].keyWindow.rootViewController error:&error];
        return true;
    } else {
    }

    return false;
}

bool Vungle::IsCachedAdAvailable()
{
    return [[VungleSDK sharedSDK] isAdPlayable];
}