#ifndef  _ADS_VUNGLE_H_
#define  _ADS_VUNGLE_H_

#include <string>

class Vungle
{
public:
   
    void static Init(const char * id);
    bool static ShowFullscreen();
    bool static IsCachedAdAvailable();
};


#endif // _ADS_VUNGLE_H_
