#include <list>
#import <iAd/iAd.h>
#include "cocos2d.h"
#include "IAdAds/iad.h"

static std::list<ADBannerView*> iadActiveBannerObjects;

@interface IAdDelegate : NSObject<ADBannerViewDelegate>
{
}
- (void)bannerViewDidLoadAd:(ADBannerView*)banner;
- (void)bannerView:(ADBannerView*)banner didFailToReceiveAdWithError:(NSError*)error;
- (BOOL)bannerViewActionShouldBegin:(ADBannerView*)banner willLeaveApplication:(BOOL)willLeave;
- (void)bannerViewActionDidFinish:(ADBannerView *)banner;
+ (void)removeBannerView:(ADBannerView*)banner;
@end

@implementation IAdDelegate
- (void)bannerViewDidLoadAd:(ADBannerView*)banner
{
    auto viewController = [UIApplication sharedApplication].keyWindow.rootViewController;
    [viewController.view addSubview:banner];
}

- (void)bannerView:(ADBannerView*)banner didFailToReceiveAdWithError:(NSError*)error
{
    NSLog(@"[IAd] bannerView didFailToReceiveAdWithError: %ld %@", (long)error.code, error.localizedDescription);
    [IAdDelegate removeBannerView:banner];
    iadActiveBannerObjects.remove(banner);
}

- (BOOL)bannerViewActionShouldBegin:(ADBannerView*)banner willLeaveApplication:(BOOL)willLeave
{
    cocos2d::Application::getInstance()->applicationDidEnterBackground();
    return YES;
}

- (void)bannerViewActionDidFinish:(ADBannerView *)banner
{
    cocos2d::Application::getInstance()->applicationWillEnterForeground();
}

+ (void)removeBannerView:(ADBannerView*)banner
{
    NSLog(@"REMOVE VIEW");
    [banner setHidden:YES];
    [banner cancelBannerViewAction];
    [banner removeFromSuperview];

    [banner.delegate release];
    banner.delegate = nil;
    [banner release];
}
@end

void IAd::Init()
{
}

void IAd::HideAll()
{
    for (ADBannerView* adView : iadActiveBannerObjects) {
        [IAdDelegate removeBannerView:adView];
    }
    iadActiveBannerObjects.clear();
}

void IAd::ShowBanner()
{
    UIInterfaceOrientation orientation = [UIApplication sharedApplication].statusBarOrientation;
    bool isPortrait = (orientation == UIInterfaceOrientationPortrait) || (orientation == UIInterfaceOrientationPortraitUpsideDown);

    ADBannerView* adView;
    // On iOS 6 ADBannerView introduces a new initializer, use it when available.
    if ([ADBannerView instancesRespondToSelector:@selector(initWithAdType:)]) {
        adView = [[ADBannerView alloc] initWithAdType:ADAdTypeBanner];
    } else {
        adView = [[ADBannerView alloc] init];
    }
    adView.delegate = [[IAdDelegate alloc] init];
    iadActiveBannerObjects.push_back(adView);

    [adView setAutoresizingMask:UIViewAutoresizingFlexibleWidth];

    // Move it to the bottom
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGRect adFrame = adView.frame;
    if (isPortrait) {
        adFrame.origin.y = screenRect.size.height - adView.frame.size.height;
    } else {
        adFrame.origin.y = screenRect.size.width  - adView.frame.size.height;
    }
    adView.frame = adFrame;
}