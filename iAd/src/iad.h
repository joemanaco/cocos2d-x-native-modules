#ifndef  _ADS_IADS_H_
#define  _ADS_IADS_H_

class IAd
{
public:
    void static Init();
    void static HideAll();
    void static ShowBanner();
};


#endif // _ADS_IADS_H_
