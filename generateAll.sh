#!/bin/bash

set -e

for i in * ; do
  if [ -d "$i" ]; then
    if [ "$i" == "bindings-generator" ]; then continue ; fi
    cd "$i"
    ./generate.sh
    cd ..
  fi
done