#include "Product.h"

#include "Manager.h"
#include <assert.h>

namespace avalon {
namespace payment {

Product::Product(const char* const productId)
: price(0)
, localizedPrice()
, localizedName()
, localizedDescription()
, purchasedCounter(0)
, manager(NULL)
, productId(std::string(productId))
{
}

Product::~Product()
{
}

std::string Product::getProductId() const
{
    return productId;
}

std::string Product::getLocalizedPrice() const
{
    return localizedPrice;
}

std::string Product::getLocalizedName() const
{
    return localizedName;
}
    
std::string Product::getLocalizedDescription() const
{
    return localizedDescription;
}


bool Product::canBePurchased() const
{
    if (!manager || !manager->isPurchaseReady()) {
        return false;
    }

    return true;
}

void Product::purchase()
{
    if (!manager) {
        assert(false && "service has to be set");
        return;
    }

    manager->purchase(getProductId().c_str());
}

void Product::onHasBeenPurchased()
{
    ++purchasedCounter;
}

bool Product::hasBeenPurchased() const
{
    return (purchasedCounter > 0);
}

void Product::consume()
{
}

} // namespace payment
} // namespace avalon
