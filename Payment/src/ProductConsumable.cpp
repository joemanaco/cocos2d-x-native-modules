#include "ProductConsumable.h"
#include <assert.h>

namespace avalon {
namespace payment {

ProductConsumable::ProductConsumable(const char* const productId, const float quantityPerPurchase)
: Product(productId)
, quantityPerPurchase(quantityPerPurchase)
{
}

ProductConsumable::~ProductConsumable()
{
    assert(getQuantity() == 0 && "unused consumable quantity detected!");
}

void ProductConsumable::consume()
{
    Product::consume();
    purchasedCounter = 0;
}

float ProductConsumable::getQuantity() const
{
    return (purchasedCounter * quantityPerPurchase);
}

float ProductConsumable::getQuantityPerPurchase() const
{
    return quantityPerPurchase;
}

} // namespace payment
} // namespace avalon
