#ifndef AVALON_PAYMENT_MANAGERDELEGATE_H
#define AVALON_PAYMENT_MANAGERDELEGATE_H

/*
#include "cocos2d.h"
#include "jsapi.h"
#include "jsfriendapi.h"
#include "ScriptingCore.h"
*/
namespace avalon {
namespace payment {

class Manager;
class Product;

class ManagerDelegate
{
    void executeJS(const char* methodName, Manager* manager, Product* product = nullptr);

public:
    virtual void onServiceStarted(Manager* const manager);
    virtual void onPurchaseSucceed(Manager* const manager, Product* const product);
    virtual void onPurchaseFail(Manager* const manager);
    virtual void onTransactionStart(Manager* const manager);
    virtual void onTransactionEnd(Manager* const manager);
    virtual void onRestoreSucceed(Manager* const manager);
    virtual void onRestoreFail(Manager* const manager);
};
} // namespace payment
} // namespace avalon

#endif /* AVALON_PAYMENT_MANAGERDELEGATE_H */
