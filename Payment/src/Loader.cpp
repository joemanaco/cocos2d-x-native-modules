#include "Loader.h"
#include <assert.h>
#include "Product.h"
#include "ProductConsumable.h"
#include "Manager.h"
#include "cocos2d.h"

namespace avalon {
namespace payment {

Manager* Loader::globalManager;

Loader::Loader(const char* iniFile)
: manager(new Manager())
, config()
{
    config.loadFile(iniFile);

    for (auto& section : *config.getSections()) {
        const char* sectionName = section.first.c_str();

        if (strcmp(sectionName, "general") == 0) {
            continue;
        }

        if (!config.getSection(sectionName)->count("type")) {
            assert(false && "Product has no type defined");
            continue;
        }
        const char* type = config.getValue(sectionName, "type");

        const char* productId = detectProductId(sectionName);
        if (productId == NULL) {
            CCLOG("Platform: %s", sectionName);
            assert(false && "Product has no productId set for this platform");
            continue;
        }

        Product *product = NULL;
        if (strcmp(type, "non-consumable") == 0) {
            product = new Product(productId);
        } else if (strcmp(type, "consumable") == 0) {
            float quantity = config.getValueAsFloat(sectionName, "quantity");
            assert(quantity > 0 && "Quantity missing or set to zero");

            product = new ProductConsumable(productId, quantity);
        } else {
            assert(false && "Couldn't identify product type from ini file");
            continue;
        }

        if (config.getSection(sectionName)->count("defaultName")) {
            product->localizedName = config.getSection(sectionName)->at("defaultName");
        }

        if (config.getSection(sectionName)->count("defaultPrice")) {
            product->localizedPrice = config.getSection(sectionName)->at("defaultPrice");
        }

        if (config.getSection(sectionName)->count("defaultDescription")) {
            product->localizedDescription = config.getSection(sectionName)->at("defaultDescription");
        }

        manager->addProduct(product, sectionName);
        CCLOG("Add Product: %s", sectionName);
    }
}

const char* Loader::detectProductId(const char* section)
{
    auto flavor = std::string("");

    #if CC_TARGET_PLATFORM == CC_PLATFORM_IOS
    auto platformName = std::string("ios");
    #elif CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID
    auto platformName = std::string("android");
    #elif CC_TARGET_PLATFORM == CC_PLATFORM_MAC
    auto platformName = std::string("mac");
    #endif

    flavor[0] = std::toupper(flavor[0]);
    auto prefix = platformName + flavor + "Id";
    CCLOG("%s", platformName.c_str());

    if (!config.getSection(section)->count(prefix.c_str())) {
        return NULL;
    }

    const char* productId = config.getValue(section, prefix.c_str());
    if (!productId || strlen(productId) == 0) {
        return NULL;
    }

    return productId;
}

Manager* Loader::getManager() const
{
    return manager;
}

} // namespace payment
} // namespace avalon
