'use strict';

var payment = payment || {
    Product: cc.Class.extend({
        ctor: function() {},        
        getLocalizedDescription: function () { return ""; },
        purchase: function () {},
        consume: function () {},
        getLocalizedPrice: function () { return "$1.99"; },
        onHasBeenPurchased: function () {},
        getProductId: function () { return ""; },
        hasBeenPurchased: function () { return false; },
        canBePurchased: function () { return false; },
        getLocalizedName: function () { return ""; },
        Product: function (char) {}
    }),
    ManagerDelegate: {
        onRestoreFail: function (manager) {},
        onTransactionEnd: function (manager) {},
        onTransactionStart: function (manager) {},
        onServiceStarted: function (manager) {},
        onRestoreSucceed: function (manager) {},
        onPurchaseFail: function (manager) {},
        onPurchaseSucceed: function (manager, product) {}
    },
    ProductConsumable: {
        /* extends Product */
        getLocalizedDescription: function () { return ""; },
        purchase: function () {},
        consume: function () {},
        getLocalizedPrice: function () { return ""; },
        onHasBeenPurchased: function () {},
        getProductId: function () { return ""; },
        hasBeenPurchased: function () { return false; },
        canBePurchased: function () { return false; },
        getLocalizedName: function () { return ""; },
        Product: function (char) {},

        getQuantity: function () { return 0; },
        getQuantityPerPurchase: function () { return 0; },
        ProductConsumable: function (char, float) {}
    },
    Manager: cc.Class.extend({
        ctor: function() {},
        getProductConsumable: function (char) { return; },
        stopService: function () {},
        getProduct: function (char) { return new payment.Product(); },
        isStarted: function () { return false;},
        isPurchaseReady: function () { return false;},
        addProduct: function(product,char) {},
        hasProduct: function (char) { return false;},
        startService: function () {},
        restorePurchases: function () {},
        purchase: function() {}
    }),
    Loader: cc.Class.extend({        
        ctor: function (char, cb) {},
        getManager: function () { return new payment.Manager(); }
    })
};
