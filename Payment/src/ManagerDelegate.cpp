#include "cocos2d.h"
#include "jsapi.h"
#include "jsfriendapi.h"
#include "ScriptingCore.h"
#include "ManagerDelegate.h"

namespace avalon {
namespace payment {

void ManagerDelegate::executeJS(const char* methodName, Manager* manager, Product* product) {
    ScriptingCore* sc =ScriptingCore::getInstance();

    JSContext* cx = sc->getGlobalContext();
    JS::RootedObject globalObject(cx, sc->getGlobalObject());

    JS::RootedValue nsval(sc->getGlobalContext());

    JS_GetProperty(cx, globalObject, "PaymentListener", &nsval);
    if (nsval !=JSVAL_VOID) {
        js_proxy_t *jsProxyManager = js_get_or_create_proxy<avalon::payment::Manager>(cx, (avalon::payment::Manager*) manager);

        auto argc = 1;
        if (product) {
            ++argc;

            js_proxy_t *jsProxyProduct = js_get_or_create_proxy<avalon::payment::Product>(cx, (avalon::payment::Product*) product);

            jsval args[2] = {
                OBJECT_TO_JSVAL(jsProxyManager->obj),
                OBJECT_TO_JSVAL(jsProxyProduct->obj),
            };

            sc->executeFunctionWithOwner(nsval, methodName, argc, args);
        } else {
            jsval args = OBJECT_TO_JSVAL(jsProxyManager->obj);
            sc->executeFunctionWithOwner(nsval, methodName, argc, &args);
        }
    }else{
        CCLOG("Undefined function: PaymentListener.%s", methodName);
    }
}

void ManagerDelegate::onServiceStarted(Manager* const manager)
{
    executeJS("onServiceStarted", manager);
};

void ManagerDelegate::onPurchaseSucceed(Manager* const manager, Product* const product)
{
    executeJS("onPurchaseSucceed", manager, product);
};

void ManagerDelegate::onPurchaseFail(Manager* const manager)
{
    executeJS("onPurchaseFail", manager);
};

void ManagerDelegate::onTransactionStart(Manager* const manager)
{
    executeJS("onTransactionStart", manager);
};

void ManagerDelegate::onTransactionEnd(Manager* const manager)
{
    executeJS("onTransactionEnd", manager);
};

void ManagerDelegate::onRestoreSucceed(Manager* const manager)
{
    executeJS("onRestoreSucceed", manager);
};

void ManagerDelegate::onRestoreFail(Manager* const manager)
{
    executeJS("onRestoreFail", manager);
};

} // namespace payment
} // namespace avalon
