#include "Manager.h"
#include <assert.h>

#include <typeinfo>
#include "Product.h"
#include "ProductConsumable.h"
#include "ManagerDelegate.h"
#include "cocos2d.h"

using std::string;
using std::make_pair;

namespace avalon {
namespace payment {

Manager::Manager()
: ignoreUnusedConsumableQuantities(false)
, delegate(new ManagerDelegate)
, started(false)
, backend(*this)
, products()
, productIdAliases()
{
}

Manager::~Manager()
{
    for (auto& pair : products) {
        auto& product = pair.second;

        if (ignoreUnusedConsumableQuantities) {
            auto consumable = dynamic_cast<ProductConsumable* const>(product);
            if (consumable) {
                consumable->consume();
            }
        }

        delete product;
    }

    products.clear();
    productIdAliases.clear();
}

void Manager::addProduct(Product* const product)
{
    if (backend.isInitialized()) {
        assert(false && "backend already initialized");
        return;
    }

    if (!product) {
        assert(false && "product must be given");
        return;
    }
    assert(!hasProduct(product->getProductId().c_str()) && "productId already in use");

    products.insert(make_pair(
        product->getProductId(),
        product
    ));
    product->manager = this;
}

void Manager::addProduct(Product* const product, const char* const alias)
{
    assert(!hasProduct(alias) && "given alias already in use");

    addProduct(product);
    productIdAliases.insert(make_pair(
        string(alias),
        product->getProductId()
    ));
}

const ProductList& Manager::getProducts() const
{
    return products;
}

Product* Manager::getProduct(const char* const productIdOrAlias) const
{
    auto productId = string(productIdOrAlias);
    auto product = products.find(productId);
    if (product != products.end()) {
        return product->second;
    }

    if (productIdAliases.count(productId) > 0) {
        auto aliasedId = productIdAliases.at(productId);
        auto aliasedProduct = products.find(aliasedId);
        if (aliasedProduct != products.end()) {
            return aliasedProduct->second;
        }
    }

    assert(false && "invalid productId or alias given");
    return NULL;
}

ProductConsumable* Manager::getProductConsumable(const char* const productIdOrAlias) const
{
    return dynamic_cast<ProductConsumable* const>(getProduct(productIdOrAlias));
}

bool Manager::hasProduct(const char* const productIdOrAlias) const
{
    auto productId = string(productIdOrAlias);

    if (products.count(productId) > 0) {
        return true;
    }

    if (productIdAliases.count(productId) == 0) {
        return false;
    }

    auto aliasId = productIdAliases.at(productId);
    return (products.count(aliasId) > 0);
}

void Manager::purchase(const char* const productIdOrAlias)
{
    if (!isPurchaseReady()) {
        assert(false && "backend service not started yet");
        return;
    }

    auto product = getProduct(productIdOrAlias);
    if (product) {
        backend.purchase(product);
    }
}

void Manager::startService()
{
    if (isStarted()) {
        assert(false && "service already started");
        return;
    }

    if (!delegate) {
        throw new std::runtime_error("payment delegate must be set!");
    }

    backend.initialize();
    started = true;
}

void Manager::stopService()
{
    backend.shutdown();
    started = false;
}

bool Manager::isStarted() const
{
    return started;
}

bool Manager::isPurchaseReady() const
{
    return (delegate && backend.isInitialized() && backend.isPurchaseReady());
}

void Manager::restorePurchases() const
{
    if (!isPurchaseReady()) {
        assert(false && "backend service not started yet");
        return;
    }

    backend.restorePurchases();
}

} // namespace payment
} // namespace avalon
