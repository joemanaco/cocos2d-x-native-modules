# Chartboost Module

The Chartboost Module is exported into the ads-namespace. In JS it's explicity not exported - so the AdManager-Module can dynamically use it on the platforms it exists (ios).

Use it like this:

```javascript
ads.CB.Init(appId, appSignature);

var result = ads.CB.ShowRewardedVideo();

result = ads.CB.ShowInterstitial();
alert(s);
```
