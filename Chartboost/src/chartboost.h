#ifndef  _ADS_CHARTBOOST_H_
#define  _ADS_CHARTBOOST_H_

#include <string>

class CB
{
public:
   
    void static Init(const char * appId, const char * signature);
    bool static ShowInterstitial();
    bool static ShowRewardedVideo();
};


#endif // _ADS_CHARTBOOST_H_
