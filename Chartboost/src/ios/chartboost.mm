#import <Chartboost/Chartboost.h>
#import "AppDelegate.h"
#import <CommonCrypto/CommonDigest.h>
#import <AdSupport/AdSupport.h>
#include "ChartboostAds/chartboost.h"

@interface ChartboostDelegateImpl : NSObject <ChartboostDelegate> {
}
- (BOOL)shouldDisplayInterstitial:(CBLocation)location;
- (void)didDisplayInterstitial:(CBLocation)location;
- (void)didCacheInterstitial:(CBLocation)location;
- (void)didDismissInterstitial:(CBLocation)location;
- (BOOL)shouldDisplayRewardedVideo:(CBLocation)location;
- (void)didDisplayRewardedVideo:(CBLocation)location;
- (void)didCacheRewardedVideo:(CBLocation)location;
- (void)didDismissRewardedVideo:(CBLocation)location;
@end

@implementation ChartboostDelegateImpl
- (BOOL)shouldDisplayInterstitial:(CBLocation)location {
    return true;
}

- (void)didDisplayInterstitial:(CBLocation)location {
}

- (void)didDismissInterstitial:(CBLocation)location {
    [Chartboost cacheInterstitial:CBLocationHomeScreen];
}

- (void)didCacheInterstitial:(CBLocation)location {
}
- (BOOL)shouldDisplayRewardedVideo:(CBLocation)location {
    cocos2d::Application::getInstance()->applicationDidEnterBackground();
    return true;
}
- (void)didDisplayRewardedVideo:(CBLocation)location {
}
- (void)didCacheRewardedVideo:(CBLocation)location {
}
- (void)didDismissRewardedVideo:(CBLocation)location {
    [Chartboost cacheRewardedVideo:CBLocationHomeScreen];
    cocos2d::Application::getInstance()->applicationWillEnterForeground();
}

@end

ChartboostDelegateImpl * chartboostDelegate;

void CB::Init(const char * appId, const char * signature) {
    chartboostDelegate = [[ChartboostDelegateImpl alloc] init];
    [Chartboost setShouldRequestInterstitialsInFirstSession:YES];

    [Chartboost startWithAppId:[NSString stringWithUTF8String:appId]
                  appSignature:[NSString stringWithUTF8String:signature]
                      delegate:chartboostDelegate];

    [Chartboost cacheInterstitial:CBLocationHomeScreen];
    [Chartboost cacheRewardedVideo:CBLocationHomeScreen];
}

bool CB::ShowInterstitial() {
    if([Chartboost hasInterstitial:CBLocationHomeScreen] == YES) {
        [Chartboost showInterstitial:CBLocationHomeScreen];
        return true;
    }
    else {
        [Chartboost cacheInterstitial:CBLocationHomeScreen];
        return false;
    }  
}

bool CB::ShowRewardedVideo() {
    if([Chartboost hasRewardedVideo:CBLocationHomeScreen] == YES) {
        [Chartboost showRewardedVideo:CBLocationHomeScreen];
        return true;
    }
    else {
        [Chartboost cacheRewardedVideo:CBLocationHomeScreen];
        return false;
    }  
}
